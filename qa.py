#!/usr/bin/python

"""
FILE qa.py

examples of objects and values
"""

if __name__ == "__main__":
    lsta = [1,2,3,4,5,6,7]
    lstb = None

    lstb = lsta
    """
        Here lsta is reference to a list (an object), lstb is alias for lista, just a different name, it still points to same object (a list).
    """

    print("---> is lsta same as lstb?")
    """
        both lists should be equivalent and point to same list
    """
    print(lsta is lstb)
    print("---> id of lsta and lstb")
    """
        lsta and lstb should have same ids (because they point to same object)
    """
    print(id(lsta),id(lstb))
    """
        OUTPUT

        ---> is lsta same as lstb?
        True
        ---> id of lsta and lstb
        (139907474698968, 139907474698968)

    """
