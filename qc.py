#!/usr/bin/python

"""
FILE qc.py
"""

def square(lst):
    """
        procedure square

        square all items in the list

        change list by reference

        arguments

        lst(list of integers)
    """
    for i in range(len(lst)):
        lst[i]=lst[i]*lst[i]

if __name__ == "__main__":
    lst = [1,2,3,4,5,6,7]
    print("---> original list")
    print(lst)
    print("---> modified list")
    square(lst)
    print(lst)
    """
        OUTPUT

        ---> original list
        [1, 2, 3, 4, 5, 6, 7]
        ---> modified list
        [1, 4, 9, 16, 25, 36, 49]

    """
