#!/usr/bin/python

"""
FILE qb.py

examples of objects, references and aliasing
"""

if __name__ == "__main__":
    lsta = [1,2,3,4,5,6,7]
    lstb = None

    lstb = lsta
    """
        Here lsta is reference to a list (an object), lstb is alias for lista, just a different name, it still points to same object (a list).
    """

    print("---> lsta")
    print(lsta)
    print("---> lstb")
    print(lstb)
    print("---> going to change list")
    lstb[0] = 8
    print(lsta)
    """
        Here both lsta and lstb point to same list ([8,2,3,4,5,6,7])
    """
    print(lstb)
    """
        OUTPUT

        ---> lsta
        [1, 2, 3, 4, 5, 6, 7]
        ---> lstb
        [1, 2, 3, 4, 5, 6, 7]
        ---> going to change list
        [8, 2, 3, 4, 5, 6, 7]
        [8, 2, 3, 4, 5, 6, 7]

    """
